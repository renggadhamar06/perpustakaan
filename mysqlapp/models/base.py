from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine(
    'mysql+mysqlconnector://root:password@localhost:3306/perpustakaan', echo=True)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()
