from sqlalchemy import Column, Integer, String
from models.base import Base


class Customers(Base):
    __tablename__ = 'customers'

    userid = Column(Integer, primary_key=True)
    username = Column(String)
    namadepan = Column(String)
    namabelakang = Column(String)
    email = Column(String)

    def __init__(self,  username, namadepan, namabelakang, email):

        self.username = username
        self.namadepan = namadepan
        self.namabelakang = namabelakang
        self.email = email
